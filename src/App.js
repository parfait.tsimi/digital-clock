import React, {Component} from 'react';
import './App.css';
import Clock from './components/Clock';
import 'bootstrap/dist/css/bootstrap.min.css';
class App extends Component{

  render(){
    return (
      <div className="container">
        <div style={{width:'520px',height:'320px', backgroundColor:'#f5f5f5', display:'flex',
            marginLeft:'20%',marginTop:'3%', border:'2px solid silver', borderRadius:'10px'}}>
          <Clock/>
        </div>
      </div>
    );
  }
  
}

export default App;
