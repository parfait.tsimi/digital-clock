import react from 'react';

function ButtonComponent(props) {

    return (
        <div>
            <button onClick={props.funct} style={{width: '90px',height: '30px',borderRadius: '10px',
            backgroundColor: '#14033D',color:'#ffffff'}}>{props.name}</button>
        </div>
    );
}

export default ButtonComponent;