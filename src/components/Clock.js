import React, {Component} from 'react';
import DayComponent from './DayComponent';
import DateComponent from './DateComponent';
import HourComponent from './HourComponent';
import FormComponent from './FormComponent';
import AMPMComponent from './AMPMComponent';
import ButtonComponent from './ButtonComponent';

class Clock extends Component{
    
    state={
        jours:new Date().getDay(),
        heures:new Date().getHours(),
        minutes:new Date().getMinutes(),
        secondes:new Date().getSeconds(),
        annees:new Date().getFullYear(),
        mois:new Date().getMonth(),
        jourDUMois:new Date().getDate(),
        ampm:'',
        start:'START',
        stop:'STOP'
    }

    handleDay = event =>{
        this.setState({
            jours: event.target.value
        });
    }
    handleChange = input =>{
        this.stopClock();
        const {name, value} = input.target
        if (value==='') {
            switch(name){
                case 'heures':
                    
                    this.setState({
                        ...this.state.heures,
                        heures:''
                    })
                    break;
                case 'minutes':
                    this.setState({
                        ...this.state.minutes,
                        minutes:''
                    })
                    break;
                case 'secondes':
                    this.setState({
                        ...this.state.secondes,
                        secondes:''
                    })
                    break;
                default:
            }
        }else{
            switch(name){
                case 'heures':
                    
                    this.setState({
                        ...this.state.heures,
                        heures:parseInt(value)
                    })
                    break;
                case 'minutes':
                    this.setState({
                        ...this.state.minutes,
                        minutes:parseInt(value)
                    })
                    break;
                case 'secondes':
                    this.setState({
                        ...this.state.secondes,
                        secondes:parseInt(value)
                    })
                    break;
                default:
                    
            }
        }
        this.startClock();
    }
    
    handleDate = event =>{
        this.setState({
            jourDUMois: event.target.value
        });
    }
    handleMonth = event =>{
        this.setState({
           mois:event.target.value
        });
    }
    handleFullYear = event =>{
        this.setState({
           annees:event.target.value
        });
    }

    setClock = () =>{
        let ss=this.state.secondes;
        let mm = this.state.minutes;
        let hh=this.state.heures;
        let jj=this.state.jours;

        this.setState({
            secondes:ss++
        })
    
        if (ss===60) {
            this.setState({
                minutes:mm+1,
                secondes:0
            })
        }
        if (mm===60) {
            this.setState({
                heures:hh+1,
                minutes:0
            })
        }  
        if (hh===24) {

            this.setState({
                jours:jj+1
            })
        }
        
        this.state.secondes++;
    }
     
    componentDidMount (){
        this.startClock();
     }
     
     componentWillUnmount(){
         this.stopClock();
     }

     stopClock=() =>{
        clearInterval(this.clockID);
    }

    startClock=() =>{ 
       this.setClock(); 
       this.clockID= setInterval(() => {
           this.setClock();
       }, 1000);
    }
    /*handleModify = ()=>{
        this.setState({
            isopenClock:false
        })
    }

    handleHide=()=>{
        this.setState({
            isopenClock:true
        })
        console.log(this.state.isopenClock)
    }*/

    render(){
            return( 
                <div style={{display: 'grid', gridTemplateColumns: 'repeat(2, 10fr)',gridColumnGap: '25px'}}>
                     <div>
                        <div style={{width:'490px',height:'290px',backgroundColor:'#000000' 
                            ,margin:'2%',color:'#ffffff'}}>
                            <DayComponent jours={this.state.jours}/>
                            <AMPMComponent ampm={this.state.heures}/>
                            <HourComponent heures={this.state.heures} minutes={this.state.minutes} 
                                secondes={this.state.secondes}/>
                            <DateComponent jourDUMois={this.state.jourDUMois} mois={this.state.mois} annees={this.state.annees}/>
                        </div>
                        {
                            <div style={{margin:'20px', paddingRight:'10px', 
                            display: 'grid',gridTemplateColumns: 'repeat(2, 1fr)'}}>
                            <ButtonComponent name={this.state.start} funct={this.startClock}/>
                            <ButtonComponent name={this.state.stop} funct={this.stopClock}/>
                        </div>}
                    </div>
                    <div style={{width:'500px',height:'550px'}}>
                        <div style={{margin:'5px'}}>
                            <FormComponent jours={this.state.jours} jourSemaine={this.handleDay}
                                annees={this.state.annees} day={this.handleDate} month={this.handleMonth}
                                    year={this.handleFullYear} hide={this.handleHide}
                                handlechange={this.handleChange}  />
                        </div>
                    </div>
                </div>    
            )        
    }
    
}

export default Clock;