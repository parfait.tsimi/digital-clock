import React from 'react';

function DayComponent (props){

    const listeJours = ["DIMANCHE","LUNDI","MARDI","MERCREDI","JEUDI","VENDREDI","SAMEDI"];
        return(
           <div style={{display:'flex',alignItems:'center',justifyContent:'center'}}>
               <span style={{fontSize:'64px'}}>{listeJours[props.jours]}</span> 
            </div>
        );
}
    
 export default DayComponent