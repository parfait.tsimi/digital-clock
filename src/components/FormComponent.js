import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

function FormComponent (props){

    const listeJours = ["DIMANCHE","LUNDI","MARDI","MERCREDI","JEUDI","VENDREDI","SAMEDI"];
    const listeMois = ["JANVIER","FEVRIER","MARS","AVRIL","MAIS","JUIN","JUILLET","AOUT","SEPTEMBRE",
    "OCTOBRE","NOVEMBRE","DECEMBRE"];
    
        return(
           <div style={{width: '400px',height: '300px',borderRadius: '20px',backgroundColor:'#000000'}}>
                <div style={{width: '400px',height: '110px',color:'#ffffff'}}>
                    <label>CHOISIR LE JOUR</label>
                    <select value={listeJours} onChange={props.jourSemaine}
                     style={{width: '120px',height: '30px',borderRadius: '20px'}}>
                        <option value="undefined"></option>
                        <option value="0">DIMANCHE</option>
                        <option value="1">LUNDI</option>
                        <option value="2">MARDI</option>
                        <option value="3">MERCREDI</option>
                        <option value="4">JEUDI</option>
                        <option value="5">VENDREDI</option>
                        <option value="6">SAMEDI</option>
                    </select>
                </div>
                {
                
                    <div className="input-wrapper">
                        <div style={{width: '400px',height: '110px',color:'#ffffff'}}>
                            <label>HEURES</label>{ }
                            <input style={{width: '60px',height: '40px',borderRadius: '20px',background: 'green',color:'#ffffff'}}
            
                            className="input" name="heures" onChange={props.handlechange} value={props.heures}/>
                        
                            <label>MINUTES</label>{ }
                            <input style={{width: '60px',height: '40px',borderRadius: '20px',background: 'green',color:'#ffffff'}} 
                            className="input" name="minutes" onChange={props.handlechange} value={props.minutes}/>
                        
                            <label>SECONDES</label>{ }
                            <input style={{width: '60px',height: '40px',borderRadius: '20px',background: 'green',color:'#ffffff'}} 
                            className="input" name="secondes" onChange={props.handlechange} value={props.secondes}/>
                        </div>
                    </div>

                }
                {
                    <div>
                        <input type="number" min="0" max="31" placeholder="JOUR DU MOIS"
                        style={{width: '135px',height: '30px',borderRadius: '20px',color:'#14033D'}} onChange={props.day}/>{  }
                        
                        <label></label>
                        <select value={listeMois} onChange={props.month} 
                        style={{width: '130px',height: '30px',borderRadius: '20px',color:'#14033D'}}>
                            <option value="undefined"></option>
                            <option value="0">JANVIER</option>
                            <option value="1">FEVRIER</option>
                            <option value="2">MARS</option>
                            <option value="3">AVRIL</option>
                            <option value="4">MAI</option>
                            <option value="5">JUIN</option>
                            <option value="6">JUILLET</option>
                            <option value="7">AOUT</option>
                            <option value="8">SEPTEMBRE</option>
                            <option value="9">OCTOBRE</option>
                            <option value="10">NOVEMBRE</option>
                            <option value="11">DECEMBRE</option>                
                        </select>
                        {  }
                        <input type="number" min="1970"  placeholder="ANNEES" 
                        style={{width: '130px',height: '30px',borderRadius: '20px',color:'#14033D'}} onChange={props.year}/>
                    </div>
                }
                {/*<ButtonComponent funct={props.hide} name={'OK'}/>*/}
            </div>
        );
}
    
 export default FormComponent