import React from 'react';

function HourComponent (props) {

    return(
        <div style={{display:'flex',alignItems:'center',justifyContent:'center',fontWeight:'bold'}}>
            <span style={{fontSize:'54px'}}>
                {(props.heures >=10)?(props.heures):("0"+ props.heures)} {' : '}</span> 
            <span style={{fontSize:'54px'}}>{(props.minutes >=10)?(props.minutes):("0"+ props.minutes)}
            {' : '}</span>
            <span style={{fontSize:'54px'}}>{(props.secondes >=10)?(props.secondes):("0"+ props.secondes)}</span>
        </div>
    );
}

export default HourComponent