import React from "react";

function AMPMComponent (props){

        return(
            <div style={{display:'flex',alignItems:'center',justifyContent:'center'}}>
                <span style={{fontSize:'34px',fontWeight:'bold'}}>
                    {(props.ampm<12)?('MATIN'):('APRES MIDI')}
                </span>
            </div>
        );
    }
    
 export default AMPMComponent