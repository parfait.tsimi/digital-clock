import React from "react";

function DateComponent (props){

    const listeMois = ["JANVIER","FEVRIER","MARS","AVRIL","MAI","JUIN", "JUILLET","AOUT","SEPTEMBRE",
        "OCTOBRE","NOVEMBRE","DECEMBRE"];
        return(
           <div style={{display:'flex',alignItems:'center',justifyContent:'center'}}>
               <span style={{fontSize:'44px',fontWeight:'bold'}}>
                    {(props.jourDUMois >= 10) ? (props.jourDUMois): "0"+(props.jourDUMois)} {  }
                {listeMois[props.mois]}{ } {props.annees}</span>
            </div>
        );
    }
    
 export default DateComponent